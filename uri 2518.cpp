#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main(){
	double n, h, c, l, area_total, hipot;
	while(true){
		cin >> n >> h >> c >> l; 
		if(!cin){
			break;
		}
		hipot = sqrt(pow(h, 2) + pow(c, 2));
		area_total =(hipot * n * l)/10000;
		
		cout << fixed << setprecision(4);
		cout << area_total << endl;
	}
	return 0;
}