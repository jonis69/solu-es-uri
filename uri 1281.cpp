#include <iostream>
#include <string.h>
#include <iomanip>

using namespace std;

struct fruta{
    char nome[100];
    double valor;
};

int main(){
    int n, qtd, m, o;
    char f[100];
    double total;
    
    cin >> n;
    for(int i = 0; i < n; i++){
        total = 0.0;
        cin >> m;
        struct fruta array[m];

        for(int j = 0; j < m; j++){
            cin >> array[j].nome >> array[j].valor;
        }
        cin >> o;
        for(int j = 0; j < o; j++){
            cin >> f >> qtd;

            for(int p = 0; p < m; p++){
                if(!strcmp(array[p].nome,f)){
                total += qtd*array[p].valor;
                break;
                }
            }
        }
        cout << fixed << setprecision(2);
        cout << "R$ " << total << endl;
    }
    return 0;
}