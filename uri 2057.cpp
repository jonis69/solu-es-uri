#include <iostream>
using namespace std;

int main(){
	int h, t, f, hora;
	
	cin >> h >> t >> f;
	if(h >= 0 && h <= 23 && t >= 1 && t <= 12 && f >= (-5) && f <= 5){
		hora  = h + t + f;
		if(hora >= 24){
			hora = hora - 24;
			cout << hora << endl;
		}
		else if(hora >= 0 && hora <= 23){
			cout << hora << endl;
		}	
		else{
			hora = hora + 24;
			cout << hora << endl;
		}
	}
	return 0;
}