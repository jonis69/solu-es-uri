#include <iostream>
#include <iomanip>
#include <cmath>
#include <cfloat>
#include <vector>

using namespace std;

struct Point{
  int x, y;
};

struct Line{
  Point Pa, Pb;
  int a, b, c;
  Line() {}
  Line(Point A, Point B){
    Pa = A; 
    Pb = B;
    a = A.y - B.y;
    b = B.x - A.x;    
    c = A.x*B.y - B.x*A.y;
  }
};

double distancia(Point P, Line L){
  return (abs(L.a*P.x + L.b*P.y + L.c)) / sqrt(pow(L.a, 2) + pow(L.b, 2));
}

bool eh_esquerda(Point c, Line L){
  Point a = L.Pa;
  Point b = L.Pb;
  return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0;
}

int main(){
  int n;
  while(true){
    cin >> n;
    if (n == 0) break;

    vector<Point> points(n);

    for (int i = 0; i < n; i++){
      int a, b;
      cin >> a >> b;
      points[i].x = a;
      points[i].y = b;
    }

    double menorDif = DBL_MAX; 
    vector<Point>::iterator I, J, A;
    for (I = points.begin(); I != points.end(); ++I){
      for (J = points.begin(); J != points.end(); ++J){
        Line L(*I, *J);
        double DL = 0;
        double DR = 0;
        for (A = points.begin(); A != points.end(); ++A){
          if (eh_esquerda(*A, L)) 
            DL += distancia(*A, L);
          else 
            DR += distancia(*A, L);
        }
        if (std::abs(DL- DR) < menorDif)
          menorDif = abs(DL - DR);
      }
    }
	cout << fixed << setprecision(3);
    cout << menorDif << endl;
  }
  return 0;
}