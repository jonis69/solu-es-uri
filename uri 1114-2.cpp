#include <iostream>
using namespace std;

int main(){
	int n;
	int x = 1, y = 1, z = 0;
	
	cin >> n;
	for (int i = 0; i <= (n * 2) - 1; i++){
		if (i % 2 == 0){
			y = x * x;
			z = x * y;
			cout << x << " " << y << " " << z << endl;
		}	
		if (i % 2 != 0){
			y = y + 1;
			z = z + 1;
			cout << x << " " << y << " " << z << endl;
			x++;
		}
	}return 0;
}

/*	
1 1 1
1 2 2
2 4 8
2 5 9
3 9 27
3 10 28
4 16 64
4 17 65
5 25 125
5 26 126*/
