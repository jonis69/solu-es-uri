#include <iostream>
using namespace std;

int main(){
	int idade, mes, anos, dias;
	
	cin >> idade;
	anos = idade/365;
	mes = (idade - (365*anos))/30;
	dias = ((idade - (365*anos)) - (30*mes));
	cout << anos << " ano(s)" << endl;
	cout << mes << " mes(es)" << endl;
	cout << dias << " dia(s)" << endl;
	return 0;
}