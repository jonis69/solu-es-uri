#include <iostream>
#include <iomanip>
using namespace std;

int main(){
	double a, b;
	cin >> a >> b;
	if(a == 1){
		cout << fixed << setprecision(2);
		cout << "Total: R$ " << (4*b) << endl;
	}
	if(a == 2){
		cout << fixed << setprecision(2);
		cout << "Total: R$ " << (4.50*b) << endl;
	}
	if(a == 3){
		cout << fixed << setprecision(2);
		cout << "Total: R$ " << (5*b) << endl;
	}
	if(a == 4){
		cout << fixed << setprecision(2);
		cout << "Total: R$ " << (2*b) << endl;
	}
	if(a == 5){
		cout << fixed << setprecision(2);
		cout << "Total: R$ " << (1.50*b) << endl;
	}
	return 0;
}