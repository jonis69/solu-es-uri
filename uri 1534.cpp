#include <iostream>

using namespace std;

int main (){
	unsigned short int tamanho, linha, coluna;

	while (cin >> tamanho){
		short matriz[tamanho][tamanho];

		for (linha = 0; linha < tamanho; linha++){
            for (coluna = 0; coluna < tamanho; coluna++){
				if (linha == coluna){
                    matriz[linha][coluna] = 1;
                }
				if (linha == tamanho - coluna - 1){
                    matriz[linha][coluna] = 2;
                }	
				if (linha != coluna && linha != tamanho - coluna - 1){
                    matriz[linha][coluna] = 3;
                }	
			}   
        }

		for (linha = 0; linha < tamanho; linha++){
			for (coluna = 0; coluna < tamanho; coluna++){
                cout << matriz[linha][coluna];
            }
			cout << endl;
		}
	}
    return 0;
}