#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main(){
	double nota1, nota2, media;
	char x;
	do{
		cin >> nota1;
		while(nota1 < 0 || nota1 > 10){
			cout << "nota invalida" << endl;
			cin >> nota1;
		}
		cin >> nota2;
		while(nota2 < 0 || nota2 > 10){
			cout << "nota invalida" << endl;
			cin >> nota2;
		}
		media = (nota1+nota2)/2;
		cout << fixed << setprecision(2);
		cout << "media = " << media << endl;
		cout << "novo calculo (1-sim 2-nao)" << endl;
		cin >> x;
		if(x == '2'){
			break;
		}
		while(x != '2' && x != '1'){
			cout << "novo calculo (1-sim 2-nao)" << endl;
			cin >> x;
		}	
	}
	while(x == '1');
	return 0;
}