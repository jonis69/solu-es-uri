#include <iostream>
#include <cmath>

using namespace std;

int main(){
	double d, vf, vg, limite = 12, tempo_f, tempo_g, dist;
	
	while(scanf("%lf", &d) != EOF){
		scanf("%lf", &vf);
		scanf("%lf", &vg);
		dist = sqrt(pow(limite, 2) + (pow(d, 2)));
		tempo_f = limite / vf;
		tempo_g = dist / vg;
		if(tempo_f >= tempo_g){
			cout << "S" << endl;
		}
		else{
			cout << "N" << endl;
		}
	}
	return 0;
}