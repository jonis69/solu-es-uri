#include <iostream>
#include <string.h>

using namespace std;

int main (){

	char palavra[101], saida[101];
	unsigned short casos, i, j, tam;

    cin >> casos;

	while (casos--){
        cin >> palavra;
		tam = strlen(palavra);

		i = tam - 1;
		j = 0;

		while (tam--){
			if (islower(palavra[i])){
                saida[j++] = palavra[i--];
            }	
			else{
                i--;
            }
		}

		saida[j] = '\0';
        cout << saida << endl;
	}
    return 0;
}