#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main(){
	int caso, qtd;
	double pct1, pct2, pct3, c = 0, r = 0, s = 0, total;
	char carac;
	cin >> caso;
	for (int i = 0; i < caso; i++){
		cin >> qtd >> carac;
		if (qtd >= 1 && qtd <= 15){
			if (carac == 'C'){
				c += qtd;
			}
			if (carac == 'R'){
				r += qtd;
			}
			if (carac == 'S'){
				s += qtd;
			}
		}
	}
	total = c + r + s;
	pct1 = (c * 100)/total;
	pct2 = (r * 100)/total;
	pct3 = (s * 100)/total;
	cout << "Total: " << total << " cobaias" << endl;
	cout << "Total de coelhos: " << c << endl;
	cout << "Total de ratos: " << r << endl;
	cout << "Total de sapos: " << s << endl;
	cout << fixed << setprecision(2);
	cout << "Percentual de coelhos: " << pct1 << " %" << endl;
	cout << "Percentual de ratos: " << pct2 << " %" << endl;
	cout << "Percentual de sapos: " << pct3 << " %" << endl;
	return 0;
}
/*Total: 92 cobaias
Total de coelhos: 29
Total de ratos: 40
Total de sapos: 23
Percentual de coelhos: 31.52 %
Percentual de ratos: 43.48 %
Percentual de sapos: 25.00 %*/
