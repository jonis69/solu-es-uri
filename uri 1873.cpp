#include <iostream>

using namespace std;

int main(){
	string s1, s2;
	
	int casos;
	
	cin >> casos;
	
	while (casos--){
        cin >> s1 >> s2;

        if (s1 == s2){
            cout << "empate" << endl;
        }      
        else if (s1 == "tesoura" && s2 == "papel"){
            cout << "rajesh" << endl;
        }
        else if (s2 == "tesoura" && s1 == "papel"){
            cout << "sheldon" << endl;
        }   
        else if (s1 == "papel" && s2 == "pedra"){
            cout << "rajesh" << endl;
        }
        else if (s2 == "papel" && s1 == "pedra"){
            cout << "sheldon" << endl;
        }   
        else if (s1 == "pedra" && s2 == "lagarto"){
            cout << "rajesh" << endl;
        }
        else if (s1 == "lagarto" && s2 == "pedra"){
            cout << "sheldon" << endl;
        }     
        else if (s1 == "lagarto" && s2 == "spock"){
            cout << "rajesh" << endl;
        }
        else if (s1 == "spock" && s2 == "lagarto"){
            cout << "sheldon" << endl;
        }     
        else if (s1 == "spock" && s2 == "tesoura"){
            cout << "rajesh" << endl;
        }
        else if (s2 == "spock" && s1 == "tesoura"){
            cout << "sheldon" << endl;
        }      
        else if (s1 == "tesoura" && s2 == "lagarto"){
            cout << "rajesh" << endl;
        }
        else if (s2 == "tesoura" && s1 == "lagarto"){
            cout << "sheldon" << endl;
        }       
        else if (s1 == "lagarto" && s2 == "papel"){
            cout << "rajesh" << endl;
        }
        else if (s1 == "papel" && s2 == "lagarto"){
            cout << "sheldon" << endl;
        }    
        else if (s1 == "papel" && s2 == "spock"){
            cout << "rajesh" << endl;
        }
        else if (s1 == "spock" && s2 == "papel"){
            cout << "sheldon" << endl;
        }   
        else if (s1 == "spock" && s2 == "pedra"){
            cout << "rajesh" << endl;
        }
        else if (s1 == "pedra" && s2 == "spock"){
            cout << "sheldon" << endl;
        }   
        else if (s1 == "pedra" && s2 == "tesoura"){
            cout << "rajesh" << endl;
        }
        else{
            cout << "sheldon" << endl;
        }
	}
    return 0;
}