#include <iostream>
#include <iomanip>

using namespace std;

int main (){

    float m[12][12], soma = 0, media = 0, cont = 0;
    int i, j;
    char operacao;

    cin >> operacao;

    for (i = 0; i < 12; i++){
        for (j = 0; j < 12; j++){
            cin >> m[i][j];
        }
    }
    if (operacao == 'S'){
        for (i = 0; i < 12; i++){
            j = 11;
            while (i != 12 - j - 1){
                soma += m[i][j];
                j--;
            }
        }
        cout << fixed << setprecision(1);
        cout << soma << endl;
    }
    else{
        if (operacao == 'M'){
            for (i = 0; i < 12; i++){
                j = 11;
                while (i != 12 - j - 1){
                    media += m[i][j];
                    j--;
                    cont++;
                }
            }
        cout << fixed << setprecision(1);
        cout << media/cont << endl;
        }
    }
    return 0;
}