#include <iostream>
#include <iomanip>

using namespace std;

int main(){
    double x[100];
    int i;

    for(i=0; i < 100; i++){
        cin >> x[i];
    }
    for(i=0; i < 100; i++){
        if(x[i] <= 10.0){
            cout << fixed << setprecision(1);
            cout << "A[" << i << "] = " << x[i] << endl;
        }
    }
    return 0;
}