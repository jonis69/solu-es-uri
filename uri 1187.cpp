#include <iostream>
#include <iomanip>

using namespace std;

int main (){
	double m[12][12], soma = 0, media = 0;
	char operacao;
	unsigned short i, j, cont = 0;

	cin >> operacao;

	for(i = 0; i < 12; i++){
		for(j = 0; j < 12; j++){
			cin >> m[i][j];
		}	
	}
	if (operacao == 'S'){

		for(i = 0; i < 12; i++){
			for(j = 0; j < 12; j++){
				if (i < j && i < 12 - j - 1){
				soma += m[i][j];
				}
			}		
		}
		cout << fixed << setprecision(1);
		cout << soma << endl;
	}

	if (operacao == 'M')
	{

		for(i = 0; i < 12; i++){
			for(j = 0; j < 12; j++){
				if (i < j && i < 12 - j - 1){
				media += m[i][j];
				cont ++;
				}
			}		
		}
		cout << fixed << setprecision(1);
		cout << media/cont << endl;
	}
	return 0;
}