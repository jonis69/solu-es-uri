#include <iostream>
#include <cmath>
using namespace std;

int main(){
	int l1, l2, l3;
	cin >> l1 >> l2 >> l3;
	if(l1 && l2 && l3 > 0 || l1 && l2 && l3 < pow(10, 5)){
		if(l1 < l2 + l3 && l2 < l1 + l3 && l3 < l1 + l2){
			if(l1 == l2 && l2 == l3){
				cout << "Valido-Equilatero" << endl;
			}
			else if(l1 == l2 || l2 == l3 || l3 == l1){
				cout << "Valido-Isoceles" << endl;
			}
			else if(l1 != l2 != l3){
				cout << "Valido-Escaleno" << endl;
			}
			if(pow(l1,2) == (pow(l2, 2) + pow(l3, 2))){
				cout << "Retangulo: S" << endl;
			} 
			else if(pow(l2,2) == (pow(l1, 2) + pow(l3, 2))){
				cout << "Retangulo: S" << endl;
			}
			else if(pow(l3,2) == (pow(l2, 2) + pow(l1, 2))){
				cout << "Retangulo: S" << endl;
			}
			else{
				cout << "Retangulo: N" << endl;
			}
		}
		else{
			cout << "Invalido" << endl;
		}
	}
	return 0;
}