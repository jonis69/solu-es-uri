#include <iostream>
 
using namespace std;
 
int qtd, matr, aux_matrc, cont;
double aux_n, nota;
 
int main(){
	cin >> qtd;
	if(qtd >= 3 && qtd <= 100){
		while(cont < qtd){
			cont = cont + 1;
			cin >> matr >> nota;
			
			if(matr > 0 && matr < 1000000 && nota >= 0 && nota <= 10){
				if(nota > aux_n){
					aux_n = nota;
					aux_matrc = matr;
				}
			}
		}	
		if(aux_n < 8.0){
			cout << "Minimum note not reached" << endl;
		}
		else{
			cout << aux_matrc << endl;
		}
	}
	return 0;
}