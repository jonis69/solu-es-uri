#include <iostream>

using namespace std;

int main(){
	int hr_inicio, min_inicio, hr_fim, min_fim, hr_total, min_total;
	
	cin >> hr_inicio >> min_inicio >> hr_fim >> min_fim;
	if(hr_inicio == hr_fim && min_inicio == min_fim){
		cout << "O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)" << endl;
	}
	if(hr_inicio == hr_fim && min_inicio != min_fim){
		min_total = min_fim - min_inicio;
		hr_total = hr_fim - hr_inicio;
		
		if(min_total < 0){
			hr_total = hr_fim - hr_inicio - 1;
			min_total = min_total + 60;
			cout << "O JOGO DUROU " << hr_total << " HORA(S) E " << min_total << " MINUTO(S)" << endl;
		}
		else{
			cout << "O JOGO DUROU " << hr_total << " HORA(S) E " << min_total << " MINUTO(S)" << endl;
		}
	}
	if(hr_inicio != hr_fim && min_inicio != min_fim){
		hr_total = hr_fim - hr_inicio;
		min_total = min_fim - min_inicio;
		
		if(hr_total < 0 && min_total < 0){
			hr_total = hr_total + 24 - 1;
			min_total = min_total + 60;
			cout << "O JOGO DUROU " << hr_total << " HORA(S) E " << min_total << " MINUTO(S)" << endl;
		}
		else if(hr_total < 0 && min_total >= 0){
			hr_total = hr_total + 24;
			cout << "O JOGO DUROU " << hr_total << " HORA(S) E " << min_total << " MINUTO(S)" << endl;
		}
		else if(hr_total >= 0 && min_total < 0){
			hr_total = hr_total - 1;
			min_total = min_total + 60;
			cout << "O JOGO DUROU " << hr_total << " HORA(S) E " << min_total << " MINUTO(S)" << endl;
		}
		else{
			cout << "O JOGO DUROU " << hr_total << " HORA(S) E " << min_total << " MINUTO(S)" << endl;
		}
	}
	if(hr_inicio != hr_fim && min_inicio == min_fim){
		hr_total = hr_fim - hr_inicio;
		min_total = min_fim - min_inicio;
		if(hr_total < 0){
			hr_total = hr_total + 24;
			cout << "O JOGO DUROU " << hr_total << " HORA(S) E " << min_total << " MINUTO(S)" << endl;
		}
		else{
			cout << "O JOGO DUROU " << hr_total << " HORA(S) E " << min_total << " MINUTO(S)" << endl;
		}
	}
	return 0;
}