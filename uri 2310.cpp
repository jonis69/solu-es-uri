#include <iostream>
#include <iomanip>

using namespace std;

int main(){
    double soma = 0.00, diferenca = 0.00, soma1 = 0.00, soma2 = 0.00;
    double diferenca2 = 0.00, diferenca1 = 0.00;
    double saque, bloqueio, ataque;
    int x, y, z;
    int t;
    int A, B, C;
    string str;
    cin >> t;

    while(t--){
        cin >> str;
        cin >> x >> y >> z;
        soma = soma + x;
        soma1 = soma1 + y;
        soma2 = soma2 + z;
        
        cin >> A >> B >> C;
        diferenca = diferenca + A;
        diferenca1 = diferenca1 + B;
        diferenca2 = diferenca2 + C;
    }
    saque = (diferenca*100.00)/soma;
    bloqueio = (diferenca1*100.00)/soma1;
    ataque = (diferenca2*100.00)/soma2;

    cout << fixed << setprecision(2);
    cout << "Pontos de Saque: " << saque << " %." << endl;
    cout << "Pontos de Bloqueio: " << bloqueio << " %." << endl;
    cout << "Pontos de Ataque: " << ataque << " %." << endl;

    return 0;
}