#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main(){
	int a, n = 1;
	cin >> a;
	if (a > 5 && a < 2000)
		while (n <= a){
			if (n % 2 == 0){
			cout << fixed << setprecision(0);
			cout << n << "^2 " << "= " << pow(n,2) << endl;
			}
	n++;}
	
	return 0;	
}
