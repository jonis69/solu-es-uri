#include <iostream>

using namespace std;

int main (){
	char string1[10010], string2[10010];
	int i, casos, soma, aux;

    cin >> casos;

	while (casos--){
        cin >> string1 >> string2;

		for (i = 0; string1[i]; i++){
			aux =  string2[i] - string1[i];
			if (aux < 0)
				soma += aux + 26;
			else
				soma += aux;
		}
        cout << soma << endl;
		soma = 0;
	}
    return 0;
}