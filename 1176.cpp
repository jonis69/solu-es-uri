#include <iostream>

using namespace std;

int main (){
    int t, n, i, k;
    long long int c = 0, a = 0, b = 1;

    cin >> t;

    for (i = 0; i < t; i++){
        cin >> n;

        if (n == 0){
            cout << "Fib(0) = 0" << endl;
        }
        else if(n == 1){
            cout << "Fib(1) = 1" << endl;
        }
        else{
            a = 0;
            b = 1;
            c = 0;

            for (k = 0; k < n; k++){
                a = b;
                b = c;
                c = a + b;
            }
            cout << "Fib(" << n << ") = " << c << endl;
        }
    }
    return 0;
}