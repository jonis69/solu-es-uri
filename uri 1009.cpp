#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

int main(){
	string nome;
	double salario, montante, total;
	
	cin >> nome >> salario >> montante;
	total = salario + (montante * 0.15);
	cout << fixed << setprecision(2);
	cout << "TOTAL = R$ " << total << endl;
	return 0;
}