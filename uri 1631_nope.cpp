#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <cfloat>
#include <vector>

using namespace std;

struct ponto{
  int x, y;
};

struct linha{
  ponto pa, pb;
  int a, b, c;
  linha() {}
  linha(ponto a, ponto b) {
    pa = A; 
    b = B;
    a = a.y - b.y;
    b = b.x - a.x;    
    c = a.x*b.y - b.x*a.y;
  }
};

double dist(ponto p, linha l) {
  return (abs(l.a*p.x + l.b*p.y + l.c)) / sqrt(pow(l.a, 2) + pow(l.b, 2));
}

bool eh_esquerda(ponto c, linha l) {
  ponto a = l.pa;
  ponto b = l.pb;
  return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0;
}

int main(){
  int n;
  while(true){
    cin >> n;
    if (n == 0){
		break;
	}
    vector<ponto> pontos(n);

    for (int i = 0; i < n; i++){
      int a, b;
      cin >> a >> b;
      pontos[i].x = a;
      pontos[i].y = b;
    }

    double menordif = DBL_MAX; 
    vector<ponto>::iterator i, j, a;
    for(i = pontos.begin(); i != pontos.end(); ++i){
      for(j = pontos.begin(); j != pontos.end(); ++j){
        linha l(*i, *j);
        double dl = 0;
        double dr = 0;
        for(a = pontos.begin(); a != pontos.end(); ++a){
          if(eh_esquerda(*a, l)) 
            dl += dist(*a, l);
          else 
            dr += dist(*a, l);
        }
        if(abs(dl- dr) < menordif){
			menordif = abs(dl - dr);
		}
      }
    }
    cout << menordif << endl;;
  }
  return 0;
}