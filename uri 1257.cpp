#include <iostream>

using namespace std;

int main(){
    int casos;
    cin >> casos;

    while(casos--){
        int n;
        cin>>n;

        int posicao=0;
        long long soma=0;

        for(int i=0; i<n; i++){
            string str;
            cin >> str;

            for(int j=0; j < str.length(); j++){
                char c=str[j];
                int diferenca=c-65;
                soma+= j + diferenca + posicao;
            }
            posicao++;
        }
        cout<< soma <<endl;
    }
    return 0;
}