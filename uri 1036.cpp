#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main(){
	double a, b, c, delta, r1, r2;
	cout << "Digite a, b e c: ";
	cin >> a >> b >> c;
	delta = pow(b, 2) - (4*a*c);
	if(delta < 0){
		cout << "Impossivel calcular" << endl;
	}
	else if(delta >= 0){
		r1 = (-b + sqrt(pow(b, 2) - 4*a*c))/(2*a);
		r2 = (-b - sqrt(pow(b, 2) - 4*a*c))/(2*a);
		if(sqrt(pow(b, 2) < 0) || 2*a == 0){
			cout << "Impossivel calcular" << endl;
		}
		else{
			cout << fixed << setprecision(5);
			cout << "R1 = " << r1 << endl;

			cout << fixed << setprecision(5);
			cout << "R2 = " << r2 << endl;
		}
	}
	return 0;
}