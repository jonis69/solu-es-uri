#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	double valor, centavos;
	int nota_100 = 0, nota_50 = 0, nota_20 = 0, nota_10 = 0, nota_5 = 0, nota_2 = 0;
	int moeda_1, moeda_50, moeda_25, moeda_10, moeda_5, moeda_1c;
	
	cin >> valor;
	nota_100 = valor/100;
	nota_50 = (valor - (100*nota_100))/50;
	nota_20 = (valor - (100*nota_100) - (50*nota_50))/20;
	nota_10 = (valor - (100*nota_100) - (50*nota_50) - (20*nota_20))/10;
	nota_5 = (valor - (100*nota_100) - (50*nota_50) - (20*nota_20) - (10*nota_10))/5;
	nota_2 = (valor - (100*nota_100) - (50*nota_50) - (20*nota_20) - (10*nota_10) - (5*nota_5))/2;
	
	centavos = (valor - ((100*nota_100) + (50*nota_50) + (20*nota_20) + (10*nota_10) + (5*nota_5) + (2*nota_2))) * 100;
	
	moeda_1 = centavos/100;
	moeda_50 = (centavos - (100*moeda_1))/50;
	moeda_25 = (centavos - (100*moeda_1) - (50*moeda_50))/25;
	moeda_10 = (centavos - (100*moeda_1) - (50*moeda_50) - (25*moeda_25))/10;
	moeda_5 = (centavos - (100*moeda_1) - (50*moeda_50) - (25*moeda_25) - (10*moeda_10))/5;
	moeda_1c = (centavos - (100*moeda_1) - (50*moeda_50) - (25*moeda_25) - (10*moeda_10) - (5*moeda_5))/1;
	
	cout << "NOTAS:" << endl;
	cout << nota_100 << " nota(s) de R$ 100.00" << endl;
	cout << nota_50 << " nota(s) de R$ 50.00" << endl;
	cout << nota_20 << " nota(s) de R$ 20.00" << endl;
	cout << nota_10 << " nota(s) de R$ 10.00" << endl;
	cout << nota_5 << " nota(s) de R$ 5.00" << endl;
	cout << nota_2 << " nota(s) de R$ 2.00" << endl;
	cout << "MOEDAS:" << endl;
	cout << moeda_1 << " moeda(s) de R$ 1.00" << endl;
	cout << moeda_50 << " moeda(s) de R$ 0.50" << endl;
	cout << moeda_25 << " moeda(s) de R$ 0.25" << endl;
	cout << moeda_10 << " moeda(s) de R$ 0.10" << endl;
	cout << moeda_5 << " moeda(s) de R$ 0.05" << endl;
	cout << moeda_1c << " moeda(s) de R$ 0.01" << endl;
	
	
	return 0;
}