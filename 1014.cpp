#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	double dist, combst, total;
	
	cin >> dist >> combst;
	
	total = dist/combst;
	
	cout << fixed << setprecision(3);
	cout << total << " km/l" << endl;
	return 0;
}