#include <iostream>

#include <bits/stdc++.h> //analizar isso depois

using namespace std;

char s[1005];
int cnt[300];

int main() {
    while (cin >> s) {
        memset(cnt, 0, sizeof(cnt));
        int n = strlen(s);

        for (int i = 0; i < n; i++){
            cnt[s[i]]++;
        }

        int impar = 0;
        for (int i = 0; i < 300; i++){
            if (cnt[i] % 2 == 1){
                impar++;
            }
        }
        
        if (impar <= 1){
            cout << "0" << endl;
        } 
        else{
            cout << impar - 1 << endl;
        } 
    }
    return 0;
}