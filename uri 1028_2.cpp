#include <iostream>

using namespace std;

int main(){
    int casos, f1, f2, temp, tam_p, aux;

    cin >> casos;
	for (int i = 0; i < casos; ++i){	
		cin >> f1 >> f2;
		if(f2 > f1){
			temp = f2;
			tam_p = f1;
		}
		else{
			temp = f1;
			tam_p = f2;
		}
		while(temp % tam_p != 0){
			aux = temp % tam_p;
			temp = tam_p;
			tam_p = aux;
		}
		printf("%d\n", tam_p);	
	}	
    return 0;
}