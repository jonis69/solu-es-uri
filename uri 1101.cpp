﻿#include <iostream>
using namespace std;

int main(){
	int m, n, a = 0, b = 0;
	while(1){
		cin >> m >> n;
		if (m <= 0 || n <= 0){
			break;
		}
		else{
			a = 0;
			b = 0;
			if (n > m){
				for(int i = m; i <= n; i++){
					a += i;
					if(i == n){
						cout << i << " Sum=" << (a) << endl;
					}
					else{
						cout << i << " ";
					}
				}
			}
			else if (m > n){
				for(int j = n; j <= m; j++){
					b += j;
					if(j == m){
						cout << j << " Sum=" << (b) << endl;
					}
					else{
						cout << j << " ";
					}
				}
			}
		}
	}
	return 0;
}