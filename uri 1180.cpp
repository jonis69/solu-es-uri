#include <iostream>
 
using namespace std;
 
int main(){
    int n, menor, pos;
    cin >> n;
    
    int x[n];

    for(int i=0; i < n; i++){
        cin >> x[i];
    }

    menor= x[0];
    pos=0;

    for(int i = 1; i < n; i++){
        if(x[i] < menor){
            pos = i;
            menor = x[i];
        }
    }
    cout << "Menor valor: " << menor << endl;
    cout << "Posicao: " << pos << endl;

    return 0;
}