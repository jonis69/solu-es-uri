#include <iostream>
#include <iomanip>

using namespace std;

int main (){

    float m[12][12], soma = 0, media = 0, elemento;
    int i, j, linha;
    char operacao;

    cin >> linha >> operacao;

    for (i = 0; i < 12; i++){
        for (j = 0; j < 12; j++){
            cin >> elemento;
            m[i][j] = elemento;
        }
    }
    if (operacao == 'S'){
        i = linha;
        while(i == linha){
            for (j = 0; j < 12; j++){
                soma += m[i][j];
            }
            i++;
        }
        cout << fixed << setprecision(1);
        cout << soma << endl;
    }
    else{
        i = linha;
        while(i == linha){
            for (j = 0; j< 12; j++){
                media += m[i][j];
            }
            i++;
        }
        cout << fixed << setprecision(1);
        cout << media/12 << endl;
    }
    return 0;
}