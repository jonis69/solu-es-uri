#include <iostream>
using namespace std;

int main(){
	
	int temp, hr, min, resto, seg;
	cin >> temp;
	hr = temp / 3600;
	resto = temp % 3600;
	min = resto / 60;
	seg = resto % 60;
	
	cout << hr << ":" << min << ":" << seg << endl;
	return 0;
}
