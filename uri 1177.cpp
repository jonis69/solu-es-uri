#include<iostream>

using namespace std;

int main(){
    int n[1000], t;

    cin >> t;
    for(int i=0, j=0; i<1000; i++, j++){
        if(j >= t){
            j = 0;
        }
        n[i] = j;
        cout << "N[" << i << "] = " << n[i] << endl;
    }
    return 0;
}